#include "common.h"

using namespace std;

string trackerLogFile = "";
int currentTracker;
vector<string> trackerAddresses;

unordered_map<string,string> loginCredentials;
unordered_map<string,bool> currentLoggedInUsers;
unordered_map<string,pair<string,set<string>>> groups; // grpId -> <admin ,members>
unordered_map<string,set<string>> pendingRequests; // grpId -> <set(userIds)>
// unordered_map<grpId,unordered_map<fileName,pair<fileMetaData*, vector<peerList*>>>>
unordered_map<string,unordered_map<string,pair<fileMetaData*, vector<peerList*>>>> fileList;

const int LISTEN_QUEUE = 8;

void clearLog(){
    ofstream out;
    out.open(trackerLogFile);
    out.clear();
    out.close();
}

void logSuccess(string msg){
    ofstream log_file(trackerLogFile, ios_base::out | ios_base::app );
    log_file << msg << endl;
    cout << msg << endl;
}

void log(bool success , string text ){
    ofstream log_file(trackerLogFile, ios_base::out | ios_base::app );
    if(success){
        string msg = "Succeded to " + text + "\n" ;
        log_file << msg;
        cout << msg;
    }else{
        string msg = "ERROR : Failed to " + text + "\n" ;
        log_file << msg;
        cout << msg;
    }
    log_file.close();
}



void handleArguments(int argc, char *argv[]){
    trackerLogFile = "trackerLog" + string(argv[2]) + ".txt";
    clearLog();
    extractTrackerInfo(trackerAddresses, string(argv[1]));
    currentTracker = stoi(string(argv[2])) - 1; // 0-based indexing
    logSuccess("Current tracker address : " + trackerAddresses[currentTracker]);
}

int peiceWiseSelection(string groupId, string fileName,vector<string>& chunkSocket){
    ll noOfInvalid = 0;
    string PERCENT_SEP(1,PERCENT);
    for(auto it = fileList[groupId][fileName].second.begin();it!=fileList[groupId][fileName].second.end();it++){
        if((*(*it)).invalid == false){
            if((*(*it)).chunkNos.size()!=0 && currentLoggedInUsers.find((*(*it)).user) != currentLoggedInUsers.end()){
                for(auto cit= (*(*it)).chunkNos.begin();cit != (*(*it)).chunkNos.end();cit++){
                    chunkSocket[stol(*cit)] = (*(*it)).ip + PERCENT_SEP + (*(*it)).port + PERCENT_SEP + (*(*it)).absPath + PERCENT_SEP;
                }
            }
        }else{
           noOfInvalid++; 
        }
    }
    if(noOfInvalid == fileList[groupId][fileName].second.size()){
        return -1;
    }
    return 0;
}

int handleCommand(vector<string>& command,string& response){
    string action = command[0];
    string CMD_DEL = string(1,COMMAND_DELIMITTER);
    int retVal=0;
    if(action == CREATE_USER){
        string userName = command[1];
        string password = command[2];
        if(loginCredentials.find(userName)!=loginCredentials.end()){
            response = FAILURE_RESPONSE_CODE + CMD_DEL + "User already exists!" + CMD_DEL ; 
            retVal = -1;

        }else{
            loginCredentials[userName] = password;
            response = CREATE_USER + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "User added successfully!" + CMD_DEL;
        }
    }
    if(action == LOGIN){
        string userName = command[1];
        string password = command[2];
        if(loginCredentials.find(userName)==loginCredentials.end()){
            response = LOGIN + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such user exists!" + CMD_DEL + BLANK_PLACHOLDER  + CMD_DEL; 
            retVal = -1;

        }else{
            if(loginCredentials[userName]!=password){
                response = LOGIN + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "Password doesn't match!" + CMD_DEL + BLANK_PLACHOLDER  + CMD_DEL ; 
                retVal = -1;
            }else {
                if(currentLoggedInUsers.find(userName)!=currentLoggedInUsers.end()){
                    if(currentLoggedInUsers[userName] == false){
                        response = LOGIN + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Login successful!!" + CMD_DEL + userName  + CMD_DEL;
                    }else{
                        response = LOGIN + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "User is already logged in a different session!" + CMD_DEL + BLANK_PLACHOLDER  + CMD_DEL;
                    }
                    currentLoggedInUsers[userName] = true;  
                }else{
                    currentLoggedInUsers.insert({userName,true});
                    response = LOGIN + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Login successful!!" + CMD_DEL + userName  + CMD_DEL;
                }
            }
        }
    }

    if(action == LOGOUT){
        string userName = command[1];
        if(currentLoggedInUsers.find(userName)!=currentLoggedInUsers.end()){
            if(currentLoggedInUsers[userName] == false){
                response = LOGOUT + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No active session for given user found!" + CMD_DEL + userName  + CMD_DEL;
            }else{
                response = LOGOUT + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Logout successful!" + CMD_DEL + userName  + CMD_DEL;
                currentLoggedInUsers[userName] = false;
            }    
        }else{
            response = LOGOUT + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No active session for given user found!" + CMD_DEL + userName  + CMD_DEL;
        }
    }

    if(action == CREATE_GROUP){
        string userName = command[1];
        string groupId = command[2];
        if(groups.find(groupId)!=groups.end()){
            response = CREATE_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "Group with same group id already exists!" + CMD_DEL;
        }else{
            set<string> grpMember;
            grpMember.insert(userName);
            groups[groupId] = make_pair(userName,grpMember);
            response = CREATE_GROUP + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Group created successfully!" + CMD_DEL;
        }
    }

    if(action == JOIN_GROUP){
        string userName = command[1];
        string groupId = command[2];
        if(groups.find(groupId)==groups.end()){
            response = JOIN_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else{
            if( groups[groupId].second.find(userName)!=groups[groupId].second.end()){
                response = JOIN_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are already member of this group!" + CMD_DEL;
            }
            else if(pendingRequests[groupId].find(userName)!=pendingRequests[groupId].end()){
                response = JOIN_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You already have a pending request which is not approved!" + CMD_DEL;
            }else{
                pendingRequests[groupId].insert(userName);
                response = JOIN_GROUP + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Request to join group is sent successfully!" + CMD_DEL;
            }
        }
    }

    if(action == LIST_PENDING_REQUEST){
        string groupId = command[2];
        if(groups.find(groupId)==groups.end()){
            response = LIST_PENDING_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(pendingRequests.size() == 0 || pendingRequests.find(groupId) == pendingRequests.end()){
            response = LIST_PENDING_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No pending request for this group!" + CMD_DEL;
        }
        else if(pendingRequests[groupId].size() == 0){
            response = LIST_PENDING_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No pending request for this group!" + CMD_DEL;
        }
        else{
            string resMsg = "\n";
            for(auto it = pendingRequests[groupId].begin();it!=pendingRequests[groupId].end();it++){
                resMsg += *it + "\n";
            } 
            response = LIST_PENDING_REQUEST + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + resMsg + CMD_DEL;
        }
    }

    if(action == ACCEPT_REQUEST ){
        string loggedInUser = command[1];
        string groupId = command[2];
        string uid = command[3];
        if(groups.find(groupId)==groups.end()){
            response = ACCEPT_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(groups[groupId].first != loggedInUser){
            response = ACCEPT_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are not the admin of this group!" + CMD_DEL;
        }
        else if(pendingRequests.find(groupId) == pendingRequests.end()){
            response = ACCEPT_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "There are no join requests made to this group!" + CMD_DEL;
        }
        else if(pendingRequests[groupId].find(uid) == pendingRequests[groupId].end()){
            response = ACCEPT_REQUEST + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "There are no join requests made to this group by this user!" + CMD_DEL;
        }
        else{
            pendingRequests[groupId].erase(uid);
            groups[groupId].second.insert(uid);
            response = ACCEPT_REQUEST + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Successfully accepted request!" + CMD_DEL;
        }
    }

    if(action == LEAVE_GROUP){
        string loggedInUser = command[1];
        string groupId = command[2];
        if(groups.find(groupId)==groups.end()){
            response = LEAVE_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(groups[groupId].first == loggedInUser){
            response = LEAVE_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You cant leave this group as you are the admin of this group!" + CMD_DEL;
        }
        else if(groups[groupId].second.find(loggedInUser)==groups[groupId].second.end()){
            response = LEAVE_GROUP + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are not part of this group!" + CMD_DEL;
        }
        else{
            groups[groupId].second.erase(loggedInUser);
            // handle removing this user from fileList
            if(fileList.find(groupId)!=fileList.end()){
                for(auto it=fileList[groupId].begin();it!=fileList[groupId].end();it++){
                    for(auto sit = it->second.second.begin();sit!=it->second.second.end();sit++){
                        if((*(*sit)).user == loggedInUser){
                            (*(*sit)).invalid = true;  
                        }
                    }
                }
            }
            response = LEAVE_GROUP + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "Successfully removed from the group!" + CMD_DEL;
        }
    }

    if(action == LIST_GROUPS){
        if(groups.size()==0){
            response = LIST_GROUPS + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No group is created yet!" + CMD_DEL;
        }else{
            string resMsg = "\nGroupId\tAdmin\n";
            for(auto it = groups.begin();it!=groups.end();it++){
                resMsg += it->first + "\t" + it->second.first + "\n";
            } 
            response = LIST_GROUPS + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + resMsg + CMD_DEL;
        }
    }
    if(action == UPLOAD_FILE){
        string loggedInUser = command[1];
        string clientIP = command[2];
        string clientPort = command[3];
        string fileName = command[4];
        string fileSize = command[5];
        string absPath = command[6];
        string numberOfChunks = command[7];
        string groupId = command[8];
        string fileSHA = command[9];

        if(groups.find(groupId)==groups.end()){
            response = UPLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(groups[groupId].second.find(loggedInUser)==groups[groupId].second.end()){
            response = UPLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are not a member of this group!" + CMD_DEL;
        }
        else {
            fileMetaData* fmd = new fileMetaData(fileSHA,fileName,fileSize,numberOfChunks);
            set<string> chunkNos;
            for(int i=0;i<stol(numberOfChunks);i++){
                chunkNos.insert(to_string(i));
            }
            peerList* pl =  new peerList(clientIP,clientPort,true,chunkNos,absPath,loggedInUser,false);
            if(fileList.find(groupId) == fileList.end()){
                vector<peerList*> peerListVector;
                peerListVector.push_back(pl);
                pair<fileMetaData*, vector<peerList*>> myPair(fmd,peerListVector);
                unordered_map<string,pair<fileMetaData*, vector<peerList*>>> fileMap;
                fileMap[fileName] = myPair;
                fileList[groupId] = fileMap;
            }
            else if(fileList[groupId].find(fileName) == fileList[groupId].end()){
                vector<peerList*> peerListVector;
                peerListVector.push_back(pl);
                pair<fileMetaData*, vector<peerList*>> myPair(fmd,peerListVector);
                fileList[groupId][fileName] = myPair;
            }
            else{
                // updating chunkNos if same file from same IP+PORT  already exixts
                string curPeerID = clientIP + clientPort + absPath + loggedInUser;
                bool doesExist = false;
                for(auto it = fileList[groupId][fileName].second.begin();it!=fileList[groupId][fileName].second.end() ; it++){
                    string itPeerID = (*(*it)).ip + (*(*it)).port + (*(*it)).absPath + (*(*it)).user;
                    if(itPeerID == curPeerID){
                        for(string chunk : chunkNos){
                            (*(*it)).chunkNos.insert(chunk);  
                            (*(*it)).isSeeder = true;  
                            (*(*it)).invalid = false;  
                        }
                        doesExist = true;
                    }
                }
                if(!doesExist){
                    fileList[groupId][fileName].second.push_back(pl);
                }
            }

            response = UPLOAD_FILE + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + "File uploaded successfully!!" + CMD_DEL;
        }
    }

    if(action == LIST_FILES){
        string loggedInUser = command[1];
        string groupId = command[2];
        if(groups.find(groupId)==groups.end()){
            response = LIST_FILES + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(groups[groupId].second.find(loggedInUser)==groups[groupId].second.end()){
            response = LIST_FILES + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are not part of this group!" + CMD_DEL;
        }
        else if(fileList.find(groupId) == fileList.end()){
            response = LIST_FILES + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No file is added to this group!" + CMD_DEL;
        }
        else{
            string res = "\n";
            string NL(1,NEW_LINE);
            for(auto it = fileList[groupId].begin();it!=fileList[groupId].end();it++){
                for(auto sit = it->second.second.begin();sit!=it->second.second.end();sit++){
                    if((*(*sit)).invalid == false){
                        res += it->first + NL;
                        break;
                    }
                }
            }
            if(res.size()!=1){
                response = LIST_FILES + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + res + CMD_DEL;
            }
            else response = LIST_FILES + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No active file are present in this group!" + CMD_DEL;
        }
    }
    if(action == STOP_SHARE){
        string loggedInUser = command[1];
        string groupId = command[2];
        string fileName = command[3];
        if(groups.find(groupId)==groups.end()){
            response = STOP_SHARE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(fileList.find(groupId) == fileList.end()){
            response = STOP_SHARE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No file is added to this group!" + CMD_DEL; 
        }
        else if(fileList[groupId].find(fileName) == fileList[groupId].end()){
            response = STOP_SHARE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No file with this file name has been added to this group!" + CMD_DEL; 
        }
        else if(groups[groupId].second.find(loggedInUser)==groups[groupId].second.end()){
            response = STOP_SHARE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are not part of this group!" + CMD_DEL;
        }
        else{
            for(auto it=fileList[groupId][fileName].second.begin();it!=fileList[groupId][fileName].second.end();it++){
                if((*(*it)).user == loggedInUser){
                    (*(*it)).invalid = true;
                }
            }
        }
    }

    if(action == DOWNLOAD_FILE){
        string loggedInUser = command[1];
        string groupId = command[2];
        string fileName = command[3];
        string destPath = command[4];
        if(groups.find(groupId)==groups.end()){
            response = DOWNLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No such group exists!" + CMD_DEL;
        }
        else if(groups[groupId].second.find(loggedInUser)==groups[groupId].second.end()){
            response = DOWNLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "You are not part of this group!" + CMD_DEL;
        }
        else if(fileList.find(groupId) == fileList.end()){
            response = DOWNLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No file is added to this group!" + CMD_DEL; 
        }
        else if(fileList[groupId].find(fileName) == fileList[groupId].end()){
            response = DOWNLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No file with this file name has been added to this group!" + CMD_DEL; 
        }
        else if(fileList[groupId][fileName].second.size() == 0){
            response = DOWNLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No seeder/leecher found for ths file!" + CMD_DEL; 
        }
        else{
            string noOfChunks = fileList[groupId][fileName].first->numberOfChunks;
            string PERCENT_SEP(1,PERCENT);
            string fileData = fileList[groupId][fileName].first->fileName + PERCENT_SEP + 
                            fileList[groupId][fileName].first->fileSize + PERCENT_SEP + 
                            fileList[groupId][fileName].first->numberOfChunks + PERCENT_SEP +
                            fileList[groupId][fileName].first->sha + PERCENT_SEP ;

            vector<string> chunkSocket(stol(noOfChunks),"");
            if(peiceWiseSelection(groupId,fileName,chunkSocket)<0){
                response = DOWNLOAD_FILE + CMD_DEL + FAILURE_RESPONSE_CODE + CMD_DEL + "No valid seeder/leecher found for ths file!" + CMD_DEL; 
            }else{
                string peerData = "";
                string AT_SEP(1,AT);
                for(string x : chunkSocket){
                    if(x==""){
                        cout << "ERROR : Something went wrong in peicewise algo!\n";
                        return -1;
                    }
                    peerData += x + AT_SEP;
                }
                response = DOWNLOAD_FILE + CMD_DEL + SUCCESS_RESPONSE_CODE + CMD_DEL + fileData + CMD_DEL + peerData + CMD_DEL + destPath +CMD_DEL+ groupId +CMD_DEL; 
                if(response.size() > COMMAND_BUFFER_SIZE){
                    cout << "ERROR : DOWNLOAD_RES_SIZE EXCEEDS COMMAND_BUFFER_SIZE\n";
                }
            }
        }
    }
    if(action == UPDATE_CHUNK){
        string loggedInUser = command[1];
        string groupId = command[2];
        string fileName = command[3];
        string ip = command[4];
        string port = command[5];
        string absAdd = command[6];
        string chunkNo = command[7];
        bool exists  = false;
        for(auto it = fileList[groupId][fileName].second.begin();it!=fileList[groupId][fileName].second.end();it++){
            if((*(*it)).ip + (*(*it)).port  + (*(*it)).absPath  == ip + port + absAdd ){
                (*(*it)).chunkNos.insert(chunkNo);
                exists = true;
            }
        }
        if(!exists){
            set<string> chunkNos;
            chunkNos.insert(chunkNo);
            peerList* pl =  new peerList(ip,port,false,chunkNos,absAdd,loggedInUser,false);
            fileList[groupId][fileName].second.push_back(pl);
        }
        cout << "Added new chunk number " + chunkNo + " to " + groupId + " " + fileName +  " \n"; 
    }

    logSuccess(response);
    while(response.size()!=COMMAND_BUFFER_SIZE){
        response.push_back(COMMAND_END_DELIMITTER);
    }

    cout << endl << "Groups" << endl;
    for(auto it = groups.begin();it!=groups.end();it++){
        cout << it->first << " " << "admin : " << it->second.first <<  endl;
        for(auto st = it->second.second.begin();st!=it->second.second.end();st++){
            cout << *st << " ";
        }
        cout << endl;
    }
    cout << endl << "Pending Requests" << endl;
    for(auto it = pendingRequests.begin();it!=pendingRequests.end();it++){
        cout << it->first<< endl;
        for(auto st = it->second.begin();st!=it->second.end();st++){
            cout << *st << " ";
        }
        cout << endl;
    }
    // unordered_map<grpId,unordered_map<fileName,pair<fileMetaData*, vector<peerList*>>>>
    // cout << endl << "File List" << endl;
    // for(auto fit = fileList.begin();fit!=fileList.end();fit++){
    //     cout << fit->first << endl;
    //     for(auto sit = fit->second.begin(); sit!=fit->second.end();sit++){
    //         cout << "\t" << sit->first << endl;
    //         cout << "\t\t" << sit->second.first->fileSize << endl;
    //         cout << "\t\t" << sit->second.first->numberOfChunks << endl;
    //         cout << "\t\t" << sit->second.first->fileName << endl;
    //         cout << "\t\t" << sit->second.first->sha << endl;
    //         for(auto tit = sit->second.second.begin(); tit != sit->second.second.end(); tit++){
    //             cout << "\t\t\t" << (*(*tit)).ip << endl;
    //             cout << "\t\t\t" << (*(*tit)).absPath << endl;
    //             cout << "\t\t\t" << (*(*tit)).port << endl;
    //             cout << "\t\t\t" << (*(*tit)).isSeeder << endl;
    //             cout << "\t\t\t" ;
    //             for(auto foit = (*(*tit)).chunkNos.begin();foit != (*(*tit)).chunkNos.end();foit++){
    //                 cout << *foit << " "  ;
    //             }
    //             cout << endl;
    //         }
    //     }
    // }

    return retVal;
}


void processCommand(string& message,int clientSocket){
    vector<string> command;
    splitString(message,command,COMMAND_DELIMITTER,COMMAND_END_DELIMITTER);
    string response;
    handleCommand(command,response); // to:do handle return values
    send(clientSocket,response.c_str(),COMMAND_BUFFER_SIZE,0);
}

int handleNewClientRequest(int clientSocket){
    cout << "In handleNewClientRequest with new thread on socket " + to_string(clientSocket) + "\n";
    char* buffer =  new char[COMMAND_BUFFER_SIZE];
    string message;
    while(1){
        message = "";
        if(readFromSocket(clientSocket,message,buffer,COMMAND_BUFFER_SIZE)<0){
            log(false,"'Lost connection from the client'");
            break;
        }
        processCommand(message,clientSocket);
    }
    delete[] buffer;
    return 0;
}

int test(){
    loginCredentials["adi"] = "raj";
    loginCredentials["damon"] = "123";
    loginCredentials["stef"] = "care";
    set<string> mySet;
    mySet.insert("adi");
    mySet.insert("damon");
    mySet.insert("stef");
    pair<string, set<string>> myPair("adi",mySet);
    groups["101"] = myPair;
    // currentLoggedInUsers["adi"] = true;
    return 0;
}

int main(int argc, char *argv[]){
    if(argc != 3){
        cout << "Provide arguments like tracker_info.txt tracker_no\n";
        return -1;
    }  
    test(); // only for testing
    handleArguments(argc,argv);
    string trackerIP="",trackerPort="";
    getTrackerIPAndPort(trackerIP,trackerPort,trackerAddresses,currentTracker);

    int trackerSocket;
    int retVal;
    trackerSocket = socket(AF_INET,SOCK_STREAM,0);
    log(trackerSocket >= 0 , "create tracker socket");

    struct sockaddr_in address;
    int addLen = sizeof(address);

    // convert IP to binary
    int localByteIP  = inet_pton(AF_INET,trackerIP.c_str(),&address.sin_addr.s_addr);
    log(localByteIP >=0 , "convert IP string to byte");

    address.sin_family = AF_INET;
    address.sin_port = htons(stoi(trackerPort));
    memset(address.sin_zero,0,8);

    retVal = bind(trackerSocket,(sockaddr*)&address,addLen);
    log(retVal >= 0 ,"bind tracker socket");

    int opt = 1;
    retVal = setsockopt(trackerSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    log(retVal >= 0 ,"set tracker socket option to reuse IP and port");

    retVal = listen(trackerSocket,LISTEN_QUEUE);
    log(retVal >=0 , "listen on port " + trackerPort + "...");

    vector<thread> workerThreads;
    while(1){
        int newSocket;
        newSocket = accept(trackerSocket,(struct sockaddr*)&address,(socklen_t*)&addLen);
        log(newSocket>=0, "accept request on a new client socket : " + to_string(newSocket));

        workerThreads.push_back(thread(handleNewClientRequest,newSocket));
    }

    for(auto th=workerThreads.begin(); th!=workerThreads.end();th++){
        if(th->joinable()) th->join();
    }
    close(trackerSocket);
    return 0;
}