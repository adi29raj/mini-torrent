#include "common.h"

void extractTrackerInfo(vector<string>& trackerAddresses,string path){
    fstream trackerInfoFile;
    trackerInfoFile.open(path.c_str(), ios::in);
    string t;
    while(getline(trackerInfoFile,t)){
        trackerAddresses.push_back(t);
    }
    trackerInfoFile.close();
}

void getTrackerIPAndPort(string& ip,string& port,vector<string>& trackerAddresses,int& currentTracker){
    string curTrackerAdd = trackerAddresses[currentTracker];
    int i=0;
    while(curTrackerAdd[i]!=':'){
        ip+=curTrackerAdd[i];
        i++;
    }
    i++;
    while(curTrackerAdd[i]!='\0'){
        port+=curTrackerAdd[i];
        i++;
    }
}

int splitString(string str,vector<string>& result,char delimitter, char endDelimitter){
    int i=0;
    string temp = "";
    while(str[i]!=endDelimitter){
        if(str[i]==delimitter){
            result.push_back(temp);
            temp = "";
        }else{
            temp+=str[i];
        }
        i++;
    }
    if(temp!=""){
        result.push_back(temp);
    }
    return 0;
}

int readFromSocket(int clientSocket,string& message,char* buffer,int LENGTH){
    int n,total=0;
    while(total < LENGTH){
        n = read(clientSocket,buffer,LENGTH);
        if(n<=0){
            close(clientSocket);
            return -1;
        }
        for(int i=0;i<n;i++){
            message.push_back(buffer[i]);
        }
        bzero(buffer,LENGTH);
        total+=n;
    }
    return 0;
}

string getWholeFileHash(string filePath){
    ostringstream stringBuf; 
    ifstream ip(filePath.c_str()); 
    stringBuf << ip.rdbuf(); 
    string contents =  stringBuf.str(), hash;
    unsigned char md[SHA256_DIGEST_LENGTH];
    if(!SHA256(reinterpret_cast<const unsigned char *>(&contents[0]), contents.length(), md)){
        cout << "ERROR  : Error in calculating SHA1\n";

    }
    else{
        for(int i=0; i<SHA256_DIGEST_LENGTH; i++){
            char buf[3];
            sprintf(buf, "%02x", md[i]&0xff);
            hash += string(buf);
        }
    }
    return hash;
}

string getStringSHA1(string str){
    unsigned char buffer[20];
    string hash = "";
    if(!SHA1(reinterpret_cast<const unsigned char *>(&str[0]), str.length(), buffer)){
        cout << "ERROR  : Error in calculating SHA1\n";
    }
    else{
        for(int i=0; i<20; i++){
            char buf[3];
            sprintf(buf, "%02x", buffer[i]&0xff);
            hash += string(buf);
        }
    }
    return hash;
}

ll numberOfChunks( ll fileSize){
    ll noOfChunks = fileSize/CHUNK_SIZE;
    if(fileSize%CHUNK_SIZE != 0){
        noOfChunks++;    
    }
    return noOfChunks;
}

string getAbsolutePath(string relPath){
  char absPath[PATH_MAX+1];
  const char *path = relPath.c_str();
  char *p = realpath(path, absPath);
  if(p==NULL){
    string msg = "Error getting absolute path(getAbsolutePath) " + relPath;
    return to_string(1);
  }
  string abs = string(p);
  return abs;
}