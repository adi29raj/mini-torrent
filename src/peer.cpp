#include "common.h"
#include "helper.h"

ofstream logFile;
const int MAX_CLIENTS = 32;
const int LISTEN_QUEUE = 8;

const int HEADER_LENGTH = 8;
const char HEADER_DELIMITTER = '#';
const char HEADER_DELIMITTER_LENGTH = 1;
const int SHA_LENGTH = 40;

int currentTracker =0; // always hit master tracker first
vector<string> trackerAddresses;
string loggedInUserId = "";
string clientIP;
int clientPort;


void logSuccess(string message){
    logFile.open("log.txt", ios::app);
    logFile  << message << endl;
    cout << message << endl;
    logFile.close();
}

void logErr(string message){
    string msg = "ERROR : " + message;
    logFile.open("log.txt", ios::app);
    logFile  << msg << endl;
    cout << msg << endl;
    logFile.close();
}

void log(bool success,string operation){
    if(!success){
        string msg = "Failed to " + operation ;
        logErr(msg);
        exit(EXIT_FAILURE);
    }else{
        string msg = "Succeded to " + operation ;
        logSuccess(msg);
    }
}

int init(string trackerInfoFilePath){
    // Clear log files if there is data in it
    logFile.open("log.txt");
    logFile.close();
    extractTrackerInfo(trackerAddresses,trackerInfoFilePath);
    logSuccess("Current tracker address : " + trackerAddresses[currentTracker]);
    return 0;
}

void splitMessage(char* buffer,vector<string>& result,int n){
    int i=0;
    string chunkNum = "";
    while(buffer[i]!=HEADER_DELIMITTER){
        chunkNum.push_back(buffer[i]);
        i++;
    }
    result.push_back(chunkNum);
    string sha = "";
    for(int k=HEADER_LENGTH;k<HEADER_LENGTH + SHA_LENGTH;k++){
        sha.push_back(buffer[k]);
    }
    result.push_back(sha);
    string message = "";
    for(int j=HEADER_LENGTH + SHA_LENGTH;j<n;j++){
        message.push_back(buffer[j]);
    }
    result.push_back(message);
}

int removeDelimitterIfLastChunk(string& str){
    int count =0;
    while(str[str.size()-1]==ESC){
        str.pop_back();
        count++;
    }
    return count;
}

void readFromClient(int clientSocket, string filepath){
    int total = 0,n;
    int totalLength = CHUNK_SIZE + HEADER_LENGTH + SHA_LENGTH;
    char message[totalLength];
    char buffer[totalLength];
    while(total < totalLength){
        n = read(clientSocket,buffer,totalLength);
        if(n<=0){
            break;          
        }
        for(int i=total,j=0;i<total+n;i++,j++){
            message[i] = buffer[j];
        }
        total+=n;
        bzero(buffer, totalLength);
    }
    vector<string> result;
    splitMessage(message,result,total);
    int chunkNum = stoi(result[0]);
    int extraCount  = removeDelimitterIfLastChunk(result[2]);

    string shaFromServer = result[1];
    string shaReceived = getStringSHA1(result[2]);
    if(shaFromServer != shaReceived){
        logErr("Corrupted chunk found!Please try again");
        return ;
    }

    fstream op(filepath.c_str(), std::fstream::in | std::fstream::out | std::fstream::binary);
    op.seekp(chunkNum*CHUNK_SIZE, ios::beg);
    if(extraCount > 0){
        op.write(result[2].c_str(), total-extraCount-HEADER_LENGTH);
    }else{
        op.write(result[2].c_str(), CHUNK_SIZE);
    }
    op.close();
}

void sendChunk(string filepath, ll chunkNum , int clientSocket){
    ifstream in(filepath.c_str(),std::ios::in|std::ios::binary);
    in.seekg(chunkNum*CHUNK_SIZE,in.beg);
    char buffer[CHUNK_SIZE] = {0};
    in.read(buffer, CHUNK_SIZE);
    int COUNT = in.gcount();

    string chunkAsString = "";
    for(int i=0;i<COUNT;i++){
        chunkAsString.push_back(buffer[i]);
    }
    string chunkSHA = getStringSHA1(chunkAsString);

    string chunkNumAsString  = to_string(chunkNum);
    int chunkNumAsStringLength = chunkNumAsString.size();
    if(chunkNumAsStringLength >= HEADER_LENGTH){
        logErr("DEV_ERROR : chunkNumAsStringLength >= HEADER_LENGTH , Number of chunks cant be more than 10^7");
        exit(EXIT_FAILURE);
    }
    while(chunkNumAsString.size()!=HEADER_LENGTH){
        chunkNumAsString.push_back(HEADER_DELIMITTER);
    }


    char header[HEADER_LENGTH];
    for(int i=0;i<HEADER_LENGTH;i++){
        header[i] = chunkNumAsString[i];
    }

    int totalLength = HEADER_LENGTH + SHA_LENGTH + COUNT;
    int TOTAL_LENGTH = HEADER_LENGTH + CHUNK_SIZE + SHA_LENGTH;
    char message[TOTAL_LENGTH];

    for(int i=0;i<HEADER_LENGTH;i++){
        message[i] = header[i];
    }
    for(int i=HEADER_LENGTH,j=0;i<HEADER_LENGTH + SHA_LENGTH;i++,j++){
        message[i] = chunkSHA[j];
    }
    for(int i=HEADER_LENGTH + SHA_LENGTH,j=0;i<totalLength;i++,j++){
        message[i] = buffer[j];
    }
    for(int i=totalLength;i<TOTAL_LENGTH;i++){
        message[i] = ESC;
    }
    send(clientSocket,message,TOTAL_LENGTH,0);
}

void initialiseFile(vector<string>& metaDataVector, string destPath){
    string filePath = destPath;
    ll fileSize = stol(metaDataVector[1]);
    ifstream f(filePath);
    if(!f.good()){
        // create empty file
        vector<char> empty(fileSize, '0');
        ofstream ofs(filePath,  std::ios::out);

        for(ll i = 0; i < fileSize; i++){
            if (!ofs.write(&empty[0], 1)){
                logErr("Issue in creating empty file");
                exit(EXIT_FAILURE);
            }
        }
        ofs.close();
    }
}

void sendMetaDataToServer(int clientSocket,string absPath, string destPath, ll chunkNo){
    string PER(1,PERCENT);
    string message = absPath + PER + to_string(chunkNo) + PER;
    while(message.size()!=COMMAND_BUFFER_SIZE){
        message.push_back(COMMAND_END_DELIMITTER);
    }
    send(clientSocket,message.c_str(),COMMAND_BUFFER_SIZE,0);
    readFromClient(clientSocket,destPath);
} 

void handleNewWorkRequest(int newSocket){
    char* buffer = new char[COMMAND_BUFFER_SIZE];
    string metaDataRcvd = "";
    if(readFromSocket(newSocket,metaDataRcvd,buffer,COMMAND_BUFFER_SIZE)<0){
        log(false,"'Error: Something wrong happened while getting response of meta data from client!'");
        return ;
    }
    vector<string> metaDataRecvdVector;
    splitString(metaDataRcvd,metaDataRecvdVector, PERCENT, END_STRING);
    sendChunk(metaDataRecvdVector[0],stol(metaDataRecvdVector[1]),newSocket);
    delete[] buffer;
}

void serve(string clientIP , int clientPort){
    cout << "In server thread function serve\n";
    int serverSocket;
    int retVal;
    serverSocket = socket(AF_INET,SOCK_STREAM,0);
    log(serverSocket >= 0 , "create server socket");

    struct sockaddr_in address;
    int addLen = sizeof(address);

    // convert IP to binary
    int localByteIP  = inet_pton(AF_INET,clientIP.c_str(),&address.sin_addr.s_addr);
    log(localByteIP >=0 , "convert IP string to byte");

    address.sin_family = AF_INET;
    address.sin_port = htons(clientPort);
    memset(address.sin_zero,0,8);

    retVal = bind(serverSocket,(sockaddr*)&address,addLen);
    log(retVal >= 0 ,"bind server socket");

    int opt = 1;
    retVal = setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    log(retVal >= 0 ,"set server socket option to reuse IP and port");

    retVal = listen(serverSocket,LISTEN_QUEUE);
    log(retVal >=0 , "listen on port " + to_string(clientPort) + "...");

    vector<thread> workerThreads;
    while(1){
        int newSocket;
        newSocket = accept(serverSocket,(struct sockaddr*)&address,(socklen_t*)&addLen);
        log(newSocket>=0, "accept request on a new worker socket : " + to_string(newSocket));

        workerThreads.push_back(thread(handleNewWorkRequest,newSocket));
    }

    for(auto th=workerThreads.begin(); th!=workerThreads.end();th++){
        if(th->joinable()) th->join();
    }
    close(serverSocket);
    return;
}

int askForFileDownload(string serverIP,int serverPort,string absPath,string destPath,ll chunkNo){
    int clientSocket;
    clientSocket = socket(AF_INET,SOCK_STREAM,0);
    log(clientSocket >= 0 , "create client socket");

    struct sockaddr_in address;
    int addLen = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(serverPort);
    memset(address.sin_zero,0,8);

    // convert IP to binary
    int localByteIP  = inet_pton(AF_INET,serverIP.c_str(),&address.sin_addr.s_addr);
    log(localByteIP >=0 , "convert IP string to byte");

    // connect 
    int retVal = connect(clientSocket,(struct sockaddr*)&address, addLen); 
    log(retVal >= 0 , "connect request to server " + serverIP + ":" + to_string(serverPort));
     
    sendMetaDataToServer(clientSocket,absPath,destPath,chunkNo);
    close(clientSocket);
    return 0;
}

int sendTrackerDownloadedChunkInfo(int trackerSocket, ll chunkNum,string grpId,string fileName,string ip,string port, string absAdd){
    string commandMessage = "";
    string CMD_DEL(1,COMMAND_DELIMITTER);
    commandMessage = UPDATE_CHUNK + CMD_DEL + loggedInUserId + CMD_DEL + grpId + CMD_DEL + fileName + 
                    CMD_DEL + ip + CMD_DEL + port + CMD_DEL +absAdd +CMD_DEL + to_string(chunkNum) + CMD_DEL ;
    while(commandMessage.size()!=COMMAND_BUFFER_SIZE){
        commandMessage.push_back(COMMAND_DELIMITTER);    
    }
    send(trackerSocket,commandMessage.c_str(),COMMAND_BUFFER_SIZE,0);
    return 0;
}

int handleDownloadFile(string fileData, string peerData, string despath,string grpId,int trackerSocket){
    vector<string> peerInfo;
    vector<string> socketInfo;
    vector<vector<string>> chunkWiseSocket;
    vector<string> metaDataVector;
    splitString(fileData,metaDataVector, PERCENT, END_STRING);
    splitString(peerData,peerInfo, AT, END_STRING);
    string destPath = despath + FILE_SEPERATOR + metaDataVector[0];

    initialiseFile(metaDataVector,destPath);
    for(string peer : peerInfo){
        socketInfo.clear();
        splitString(peer,socketInfo, PERCENT, END_STRING);
        chunkWiseSocket.push_back(socketInfo);
    }
    vector<thread> downloadThreads;
    for(ll chunkNo = 0;chunkNo < chunkWiseSocket.size() ;chunkNo++){
        askForFileDownload(chunkWiseSocket[chunkNo][0],stoi(chunkWiseSocket[chunkNo][1]),chunkWiseSocket[chunkNo][2],destPath, chunkNo);
        downloadThreads.push_back(thread(sendTrackerDownloadedChunkInfo,trackerSocket,chunkNo,grpId,metaDataVector[0],clientIP,to_string(clientPort),destPath));
    }
    for(auto th=downloadThreads.begin(); th!=downloadThreads.end();th++){
        if(th->joinable()) th->join();
    }
    logSuccess("File downloaded...");

    return 0;       
}

int processResponse(string& response,int trackerSocket){
    if(response ==""){
        logErr("No response from tracker");
        return -1;
    }
    vector<string> resVector;
    splitString(response,resVector,COMMAND_DELIMITTER,COMMAND_END_DELIMITTER);
    if(resVector[1] == SUCCESS_RESPONSE_CODE){
        logSuccess("TRACKER : " + resVector[2]);
        if(resVector[0] == LOGIN){
            loggedInUserId = resVector[3];
        }
        if(resVector[0] == LOGOUT){
            loggedInUserId = "";
        }
        if(resVector[0] == DOWNLOAD_FILE){
            handleDownloadFile(resVector[2],resVector[3],resVector[4],resVector[5],trackerSocket);
        }
    }else{
        logErr("TRACKER : " + resVector[2]);
    }
    return 0;
}

int sendToTracker(int trackerSocket){
    string commandMessage;
    int retVal;
    char* buffer = new char[COMMAND_BUFFER_SIZE];
    string response = "";
    while(1){
        retVal = processUserCommand(loggedInUserId,clientIP,clientPort,commandMessage);
        if(retVal < 0){
            continue;
        }
        send(trackerSocket,commandMessage.c_str(),COMMAND_BUFFER_SIZE,0);
        response = "";
        if(readFromSocket(trackerSocket,response,buffer,COMMAND_BUFFER_SIZE)<0){
            log(false,"'Error: Something wrong happened while getting response from tracker!'");
            break;
        }
        processResponse(response,trackerSocket);
    }
    delete[] buffer;
    return 0;
}

int connectToTracker(){
    int trackerSocket;
    trackerSocket = socket(AF_INET,SOCK_STREAM,0);
    log(trackerSocket >= 0 , "create connect to tracker socket");

    string trackerIP="",trackerPort="";
    getTrackerIPAndPort(trackerIP,trackerPort,trackerAddresses,currentTracker);

    struct sockaddr_in address;
    int addLen = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(stoi(trackerPort));
    memset(address.sin_zero,0,8);

    // convert IP to binary
    int localByteIP  = inet_pton(AF_INET,trackerIP.c_str(),&address.sin_addr.s_addr);
    log(localByteIP >=0 , "convert IP string to byte");

    // connect 
    int retVal = connect(trackerSocket,(struct sockaddr*)&address, addLen); 
    log(retVal >= 0 , "connect request to tracker " + trackerIP + ":" + trackerPort);

    sendToTracker(trackerSocket);
    close(trackerSocket);
    return 0;
}

int main(int argc, char* argv[]){
    if(argc!=4){
        cout << "Provide arguments like <IP>:<PORT> tracker_info.txt" << endl;
        return -1;
    }
    init(string(argv[3]));

    clientIP = argv[1];
    clientPort = stoi(argv[2]);
    cout << clientIP << clientPort << endl;
    thread serverThread(serve ,clientIP ,clientPort);
    thread trackerThread(connectToTracker);

    trackerThread.join();
    serverThread.join();
    return 0;
}