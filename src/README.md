To compile and execute tracker
    g++ tracker.cpp common.cpp -lssl -lcrypto -o tracker && ./tracker tracker_info.txt 1

To compile and execute peer
    g++ peer.cpp helper.cpp common.cpp -lssl -lcrypto -o peer && ./peer 127.0.0.1 2002 tracker_info.txt

COMMAND_RESPONSE = ACTION + "#" + FAILURE_RESPONSE_CODE + # + MESSAGE + # + EXTRA_INFO/PLACEHOLDER  + # + $$$$$$...; 