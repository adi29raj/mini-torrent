#include "helper.h"
#include "common.h"

vector<string> cmd;

string chunkWiseSHA(string filePath, string CMD_DEL,ll fileSize){
    string hash = "";
    string chunkHash;
    ll noOfChunks = numberOfChunks(fileSize);
    char buffer[CHUNK_SIZE] = {0};
    for(ll chunkNum=0;chunkNum<noOfChunks;chunkNum++){
        ifstream in(filePath.c_str(),std::ios::in|std::ios::binary);
        in.seekg(chunkNum*CHUNK_SIZE,in.beg);
        in.read(buffer, CHUNK_SIZE);
        int COUNT = in.gcount();
        chunkHash = "";
        for(int i=0;i<COUNT;i++){
            chunkHash.push_back(buffer[i]);    
        }
        hash+= getStringSHA1(chunkHash) + CHUNK_HASH_DELIMITTER;
    }
    return hash;
}

int handleUploadFile(vector<string>& command,string loggedInUser,string clientIP,int clientPort,string &message){
    string filePath = command[1];
    string grpId = command[2];
    vector<string> filePathVector;
    splitString(filePath,filePathVector,FILE_SEPERATOR,END_STRING);
    string fileName = filePathVector.back();
    string CMD_DEL(1,COMMAND_DELIMITTER);

    ifstream in(filePath.c_str(),std::ios::in|std::ios::binary|ios::ate);
    ll fileSize = in.tellg();
    if(!in.good()){
        cout << "ERROR : No such file found!";
        return -1;
    }
    if(fileSize<=0){
        cout << "ERROR : File is empty!";
        return -1;
    }
    in.close();
    ll noOfChunks = numberOfChunks(fileSize);
    string sha = getWholeFileHash(filePath);
    string absPath = getAbsolutePath(filePath);
    if(absPath == to_string(1)){
        cout << "ERROR: Error in getting absolute path for the given file\n";
    }
    fileMetaData fmd(sha,fileName,to_string(fileSize),to_string(noOfChunks));
    message = UPLOAD_FILE + CMD_DEL + loggedInUser +  CMD_DEL + clientIP + CMD_DEL + to_string(clientPort) + CMD_DEL +
                fmd.fileName + CMD_DEL + fmd.fileSize + CMD_DEL + absPath + CMD_DEL + fmd.numberOfChunks + CMD_DEL + grpId + CMD_DEL + 
                fmd.sha + CMD_DEL;
                // + chunkWiseSHA(filePath,CMD_DEL,fileSize) + CMD_DEL;

    if(message.size()>=COMMAND_BUFFER_SIZE){
        cout << "ERROR : Command buffer overflow!\n";
        return -1;
    }
    while(message.size()!=COMMAND_BUFFER_SIZE){
        message.push_back(COMMAND_END_DELIMITTER);
    }

    return 0;
}

int handleUserCommand(vector<string>& command,string loggedInUser,string clientIP,int clientPort,string &message){
    string action = command[0];
    message = "";
    string CMD_DEL(1,COMMAND_DELIMITTER);
    if(action == "create_user"){
        if(loggedInUser!=""){
            cout << "ERROR : Already logged in!\n";
            return -1;
        }
        if(command.size()!=3){
            cout << "ERROR : Provide arguments like create_user <user_id> <password>!\n";
            return -1;
        }
        message = CREATE_USER + CMD_DEL  + command[1] + CMD_DEL + command[2] + CMD_DEL;
    }
    else if( action == "login"){
        if(loggedInUser!=""){
            cout << "ERROR : Already logged in!\n";
            return -1;
        }
        if(command.size()!=3){
            cout << "ERROR : Provide arguments like login <user_id> <password>!\n";
            return -1;
        }
        message = LOGIN + CMD_DEL  + command[1] + CMD_DEL + command[2] + CMD_DEL;
    }
    else if( action == "logout"){
        if(loggedInUser==""){
            cout << "ERROR : No one is logged in currently!\n";
            return -1;
        }
        if(command.size()!=1){
            cout << "ERROR : Provide arguments like logout!\n";
            return -1;
        }
        message = LOGOUT + CMD_DEL + loggedInUser +  CMD_DEL;
    }
    else if( action == "create_group"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before creating a group\n";
            return -1;
        }
        if(command.size()!=2){
            cout << "ERROR : Provide arguments like create_group <group_id>!\n";
            return -1;
        }
        message = CREATE_GROUP + CMD_DEL + loggedInUser +  CMD_DEL + command[1];
    }
    else if( action == "join_group"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before joining a group\n";
            return -1;
        }
        if(command.size()!=2){
            cout << "ERROR : Provide arguments like join_group <group_id>!\n";
            return -1;
        }
        message = JOIN_GROUP + CMD_DEL + loggedInUser +  CMD_DEL + command[1];
    }
    else if( action == "leave_group"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before joining a group\n";
            return -1;
        }
        if(command.size()!=2){
            cout << "ERROR : Provide arguments like leave_group <group_id>!\n";
            return -1;
        }
        message = LEAVE_GROUP + CMD_DEL + loggedInUser +  CMD_DEL + command[1];
    }
    else if( action == "list_requests"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=2){
            cout << "ERROR : Provide arguments like list_requests<group_id>!\n";
            return -1;
        }
        message = LIST_PENDING_REQUEST + CMD_DEL + loggedInUser +  CMD_DEL + command[1];
    }
    else if( action == "accept_request"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=3){
            cout << "ERROR : Provide arguments like accept_request <group_id> <user_id>!\n";
            return -1;
        }
        message = ACCEPT_REQUEST + CMD_DEL + loggedInUser +  CMD_DEL + command[1] + CMD_DEL +  command[2];
    }
    else if( action == "list_groups"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=1){
            cout << "ERROR : Provide arguments like list_groups!\n";
            return -1;
        }
        message = LIST_GROUPS + CMD_DEL + loggedInUser +  CMD_DEL;
    }
    else if(action == "upload_file"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=3){
            cout << "ERROR : Provide arguments like upload_file <file_path> <group_id>!\n";
            return -1;
        }
        return handleUploadFile(command,loggedInUser,clientIP,clientPort,message);
    }
    else if(action == "list_files"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=2){
            cout << "ERROR : Provide arguments like list_files <group_id>!\n";
            return -1;
        }
        message = LIST_FILES + CMD_DEL + loggedInUser +  CMD_DEL + command[1] + CMD_DEL;
    }
    else if(action == "stop_share"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=3){
            cout << "ERROR : Provide arguments like stop_share <group_id> <file_name>!\n";
            return -1;
        }
        message = STOP_SHARE + CMD_DEL + loggedInUser +  CMD_DEL + command[1] + CMD_DEL + command[2] + CMD_DEL;
    }
    else if(action == "download_file"){
        if(loggedInUser==""){
            cout << "ERROR : Please login before making this request\n";
            return -1;
        }
        if(command.size()!=4){
            cout << "ERROR : Provide arguments like download_file <group_id> <file_name> <destination_path>!\n";
            return -1;
        }
        string absPath = getAbsolutePath(command[3]);
        if(absPath == to_string(1)){
            cout << "ERROR: Error in getting absolute path for the given file\n";
        }
        message = DOWNLOAD_FILE + CMD_DEL + loggedInUser +  CMD_DEL + command[1] + CMD_DEL + command[2] + CMD_DEL + absPath + CMD_DEL;

    }
    else{
        cout << "ERROR : Enter correct command!\n";
        return -1;
    }

    if(message.size()>=COMMAND_BUFFER_SIZE){
        cout << "ERROR : Command buffer overflow!\n";
        return -1;
    }
    while(message.size()!=COMMAND_BUFFER_SIZE){
        message.push_back(COMMAND_END_DELIMITTER);
    }
    return 0;
}

int processUserCommand(string loggedInUser,string clientIP,int clientPort,string &message){
    cout << "\nEnter command\n";
    string userCommand="";
    getline(cin, userCommand);
    if(userCommand!=""){
        cmd.clear();
        splitString(userCommand,cmd,SPACE,END_STRING);
        return handleUserCommand(cmd,loggedInUser,clientIP,clientPort,message);
    }else{
        cout << "ERROR : Enter some command you fool!\n";
        return -1;
    }
    return 0;
}