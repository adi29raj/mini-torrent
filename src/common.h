#ifndef COMMON_HEADER 
#define COMMON_HEADER

#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <fstream>
#include <unistd.h>
#include <bits/stdc++.h>
#include <thread>
#include <openssl/sha.h>
using namespace std;

#define ll long long

const ll CHUNK_SIZE = 524288;
const ll COMMAND_BUFFER_SIZE = 131072;

const char COMMAND_DELIMITTER = '#';
const char COMMAND_END_DELIMITTER = '$';
const char SPACE = ' ';
const char NEW_LINE = '\n';
const char END_STRING = '\0';
const char FILE_SEPERATOR = '/';
const char CHUNK_HASH_DELIMITTER = '%';
const char PERCENT = '%';
const char AT = '@';
const char ESC = '^';

const string CREATE_USER = "0";
const string LOGIN = "1";
const string LOGOUT = "2";
const string CREATE_GROUP = "3";
const string JOIN_GROUP = "4";
const string LEAVE_GROUP = "5";
const string ACCEPT_REQUEST = "6";
const string LIST_PENDING_REQUEST = "7";
const string LIST_GROUPS = "8";
const string UPLOAD_FILE = "9";
const string LIST_FILES = "10";
const string STOP_SHARE = "11";
const string DOWNLOAD_FILE = "12";
const string SHOW_DOWNLOADS = "13";

const string UPDATE_CHUNK = "14";

const string BLANK_PLACHOLDER = "--";

const string SUCCESS_RESPONSE_CODE = "200";
const string FAILURE_RESPONSE_CODE = "400";

struct fileMetaData{
    string sha;
    string fileName;
    string fileSize;
    string numberOfChunks;
    fileMetaData(string _sha,string _fileName, string _fileSize, string _numberOfChunks){
        sha = _sha;
        fileName = _fileName;
        fileSize = _fileSize;
        numberOfChunks = _numberOfChunks;
    }
};

struct peerList{
    string ip;
    string port;
    bool isSeeder;
    set<string> chunkNos;
    string absPath;
    string user;
    bool invalid;
    peerList(string _ip, string _port, bool _isSeeder, set<string> _chunkNos,string _absPath,string _user,bool _invalid){
        string CMD_DEL(1,COMMAND_DELIMITTER);
        ip=_ip;
        port=_port;
        isSeeder = _isSeeder;
        chunkNos = _chunkNos;
        absPath = _absPath;
        user = _user;
        invalid = _invalid;
    }
};

void getTrackerIPAndPort(string& ip,string& port,vector<string>& trackerAddresses,int& currentTracker);
void extractTrackerInfo(vector<string>& trackerAddresses,string path);
int splitString(string str,vector<string>& result,char delimitter, char endDelimitter);
int readFromSocket(int clientSocket,string& message,char* buffer,int LENGTH);
string getWholeFileHash(string filePath);
string getStringSHA1(string str);
ll numberOfChunks(ll fileSize);
string getAbsolutePath(string relPath);

#endif